#!/bin/bash -
#===============================================================================
#
#          FILE: install.sh
#
#         USAGE: ./install.sh [[ -h]|[-l]|[-f] [-c destination] [-d common config] [-t status bar theme name]]
#
#   DESCRIPTION: install tmux configuration
#
#       OPTIONS:
#               -h, --help     help
#               -l, --list     list of available status bar hemes
#               -q, --quiet    do not ask for user's input
#               -f, --force    override destination config file if it exists
#               -c, --config   destination config file. Default: ${HOME}/.tmux.conf
#               -m, --common   file path which contains common configs.
#                              Default: common.tmux.conf (in same directory
#                              with this file)
#               -t, --theme    status bar theme to use. Use -l for list of
#                              available themes. If specify unexisten theme name
#                              then a themewon't included at all. Default: simple.
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Alex Lov (alex@alexlov.com)
#  ORGANIZATION:
#       CREATED: 07/01/2018 20:06
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

## ERRORS
## Codes from /usr/include/sysexits.h
E_USAGE=64
E_DATAERR=65
E_NOINPUT=66
E_CANTCREAT=73
E_CONFIG=78

## Internally used variables
ROOT_DIR="$(cd "$(dirname "$0")" ; pwd -P)"
THEMES_DIR="${ROOT_DIR}/themes"
OS_DIR="${ROOT_DIR}/os"

## Defaults
SHOW_HELP=false
SHOW_THEMES=false
OVERRIDE_CONFIG=false
QUIET=false
CONFIG_FILE="${HOME}/.tmux.conf"
COMMON_CONF_FILE="${ROOT_DIR}/common.tmux.conf"
THEME="simple"
THEME_CONF_FILE="${THEMES_DIR}/${THEME}.tmux.conf"

## Get a file with OS specific params
OS_CONF_FILE=""
case $(uname -s 2>/dev/null) in
    Linux)
        [[ -f "${OS_DIR}/linux.tmux.conf" ]] && \
            OS_CONF_FILE="${OS_DIR}/linux.tmux.conf"
        ;;
    Darwin)
        [[ -f "${OS_DIR}/macos.tmux.conf" ]] && \
            OS_CONF_FILE="${OS_DIR}/macos.tmux.conf"
        ;;
esac

## Echo to stderr instead of stdout
echoerr() {
    printf "%s\n" "$*" >&2
}

## Show help for the script
usage() {
cat <<End-of-message
Usage: $0 [options]

Options:
  -h, --help      help
  -l, --list      list of available status bar themes
  -q, --quiet     do not ask for user's input
  -f, --force     override destination config file if it exists
  -c, --config    destination config file.
                  Default: $CONFIG_FILE
  -m, --common    file path which contains common config.
                  Default: $COMMON_CONF_FILE
  -t, --theme     status bar theme to use. Use -l for list of available themes.
                  Default: $THEME. If specify unexisten theme name
                  then it won't included at all.
End-of-message
}

GETOPTS_STRING=":hlfqc:m:t:"
parse_args() {
    case "$1" in
        ## Square brackets are important!
        [-h]|--help) SHOW_HELP=true;;
        [-l]|--list) SHOW_THEMES=true;;
        [-f]|--force) OVERRIDE_CONFIG=true;;
        [-q]|--quiet) QUIET=true;;
        [-c]|--config) CONFIG_FILE="$2";;
        [-m]|--common) COMMON_CONF_FILE="$2";;
        [-t]|--theme) THEME_CONF_FILE="${THEMES_DIR}/$2.tmux.conf";;
        :) echo "Missed an argument for option '-${2}'"; usage; exit $E_USAGE;;
        \?) echo "Invalid option: '-${2}'"; usage; exit $E_USAGE;;
        *) echo "Unknown option: '$1'"; usage; exit $E_USAGE;;
    esac
}

## Parse cli flags/arguments
parse_flags() {
    local args="$*"
    local flag
    set +o nounset
    while [[ ${#args} -gt 0 ]]; do
        first="${args%% -*}"
        args="$(echo ${args#$first}|xargs)" ## xargs for trimming whitespaces
        case $first in
            --*=*) parse_args "${first%%=*}" "${first#*=}";;
            --*) parse_args "${first%% *}" "${first#* }";;
            -*)
                OPTIND=1
                while getopts "$GETOPTS_STRING" flag $first; do
                    parse_args "$flag" "$OPTARG"
                done
                ;;
            *) parse_args $first;;
        esac
    done
    set -o nounset
}

## Return list of available status bar themes
list_themes() {
    for file in $(ls "$THEMES_DIR"); do
        echo ${file%%.*}
    done
}

## Erase destination config file
erase_config_file() {
    local override
    local erase_config=false
    if [[ -f $CONFIG_FILE ]]; then
        if $OVERRIDE_CONFIG; then
            erase_config=true
        elif ! $QUIET; then
            ## Ask a user that (s)he wants to override a config file
            read -p "Override file $CONFIG_FILE? [y/N]: " override
            [[ $override == [yY] ]] && erase_config=true
        fi
    else
        erase_config=true
    fi
    if ! $erase_config; then
        ! $QUIET && \
            echoerr "[WARNING] File $CONFIG_FILE exists and overriding of it was not allowed"
        return $E_CANTCREAT
    fi
    echo -n '' > "$CONFIG_FILE"
}

## Returns snippet for tmux.conf which includes given file path
tmux_include() { # $1 - including file path
    local file=${1:-}
    [[ -z $file ]] && {
        echoerr "[ERROR] tmux_include(): file path is not specified"
        return $E_USAGE
    }
    local filename=$(basename $file)
    [[ -z $filename ]] && {
        echoerr "[ERROR] tmux_include(): could not extract file name from '$file'"
        return $E_DATAERR
    }

    printf '# Include %s\n' "$filename"
    ## Stupid repeating is just for compatibility
    printf 'if-shell "test -f %s" "source %s"\n' "$file" "$file"
}

## Add include statement with $1 file to $2 config file
include_file() { # $1 - file to include to tmux.conf, $2 - destination config file
    local file=${1:-}
    local config_file=${2:-"$CONFIG_FILE"}
    [[ -z $file ]] && {
        echoerr "[ERROR] include_file(): file path to include is not specified"
        return $E_USAGE
    }
    [[ -f $file ]] || {
        echoerr "[ERROR] include_file(): '$file' does not exist"
        return $E_NOINPUT
    }
    [[ -z $config_file ]] && {
        echoerr "[ERROR] include_file(): config file is not specified"
        return $E_USAGE
    }
    [[ -f $config_file ]] || {
        echoerr "[ERROR] include_file(): '$config_file' does not exist"
        return $E_NOINPUT
    }
    local include_conf=$(tmux_include "$file")
    local include_conf_rc=$?
    if [[ $include_conf_rc -eq 0 ]]; then
        ## Add an empty line if config is not empty just for readability
        [[ -s $config_file ]] && printf '\n' >> "$config_file"
        echo -n "$include_conf" >> "$config_file"
    fi
    return $include_conf_rc
}

##### Main #####
## Exit in case of any errors during parsing input arguments
parse_flags "$@" || exit $?

## Show usage and exit
$SHOW_HELP && usage && exit $?

## Show list of themes and exit
$SHOW_THEMES && list_themes && exit $?

## Exit if we cannot (or are not allowed to) erase/override config file
erase_config_file || exit $?

## Include common params
## Exit as config file does not make sense without common part
if [[ -n $COMMON_CONF_FILE ]]; then
    include_file "$COMMON_CONF_FILE" || exit $?
else
    echoerr "[ERROR] There is no file with common parameters specified"
    exit $E_CONFIG
fi

## Include file with OS specific params
[[ -n $OS_CONF_FILE ]] && include_file "$OS_CONF_FILE"

## Include file with status bar theme
[[ -n $THEME_CONF_FILE ]] && include_file "$THEME_CONF_FILE"
echo '' >> $CONFIG_FILE
