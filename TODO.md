- Tmux plugins:
  * https://github.com/tmux-plugins/tpm — plugins installer
  * https://github.com/tmux-plugins/tmux-net-speed — show net speed, does not
    support macos yet
  * https://github.com/tmux-plugins/tmux-battery - battery status
  * https://github.com/tmux-plugins/tmux-resurrect - save/restore tmux
    environmet over restarts, restores "some" applications
  * https://github.com/tmux-plugins/tmux-maildir-counter — mail counter
  * https://github.com/tmux-plugins/tmux-continuum - (?) *automatically*
    save/restore tmux environment, depends on tmux-resurrect
  * https://github.com/tmux-plugins/tmux-cpu - CPU/GPU usage
  * https://github.com/dominikduda/tmux_mode_indicator or
    https://github.com/tmux-plugins/tmux-prefix-highlight -
    plugins to highlight normal/prefix/copy modes. Need to choose one and adapt
    if needed
